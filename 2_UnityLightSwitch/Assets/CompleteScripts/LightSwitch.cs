﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener trigger;
    public Image cursorImage;

    //set up variable and get reference to light
    public Light spotLight;

    //set up variable and get reference to sound
    public AudioSource audioSource;

    //set up variable and get reference to animation
    public Animation anim;

    // Use this for initialization
    void Start ()
    {
        cursorImage.enabled = false;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
	}

	// Update is called once per frame
	void Update ()
    {
        Debug.Log("Update was called");
    }

    void OnMouseOver()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        else
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseExit()
    {
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audioSource.Play();

            anim.Stop();
            anim.Play();

            Debug.Log("Switch Pressed");
            if (spotLight.intensity > 0f)
            {
                spotLight.intensity = 0f;
            }
            else
            {
                spotLight.intensity = 1.95f;
            }
        }
    }


}
