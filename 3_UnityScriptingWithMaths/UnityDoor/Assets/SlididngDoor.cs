﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlididngDoor : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;

    public float openAmount = 1f;
    public float speed = 1f;

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", openAmount);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }

    IEnumerator DoorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        Debug.Log("OpenStart");
        while (xPos < (target - 0.02f) || xPos > (target + 0.02f))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Opening");
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Finished");
        yield return null;
    }

}
